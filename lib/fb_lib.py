import requests,time,json
from requests.packages.urllib3.exceptions import InsecureRequestWarning;requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

def init():
	r = requests.get("https://www.facebook.com/",headers={"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)","Connection": "keep-alive"},verify=False).text.encode("utf-8")
	rev=r[r.find("client_revision")+17:r.find("client_revision")+24]
	return rev

def login(email,password):
	h={
	"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)",
	"Content-Type": "application/x-www-form-urlencoded",
	"Cookie": "wd=0x0","Connection": "keep-alive"}
	r=requests.post("https://www.facebook.com/login.php",headers=h,data="email="+email+"&pass="+password+"",verify=False,allow_redirects=False)
	c_user=r.cookies["c_user"]
	sb=r.cookies["sb"]
	xs=r.cookies["xs"]
	#timestamp=xs[26:36]
	fr=r.cookies["fr"]
	pl=r.cookies["pl"]
	return c_user,sb,xs,fr,pl

def welcome(c_user,xs):
	r=requests.get("https://www.facebook.com/?sk=welcome",headers={"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)","Connection": "keep-alive","Cookie": "c_user="+c_user+"; xs="+xs+""},verify=False).text.encode("utf-8")
	return r[r.find("fb_dtsg")+16:r.find("fb_dtsg")+41]

def pull_zero(c_user,fb_dtsg,xs):
	r = requests.get("https://www.facebook.com/ajax/presence/reconnect.php?__user="+c_user+"&__a=1&fb_dtsg="+fb_dtsg+"",verify=False,headers={"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)","Connection": "keep-alive","Cookie":"c_user="+c_user+"; xs="+xs+";"}).text.encode("utf-8")
	json_r=json.loads(r[9:])["payload"]
	seq=str(json_r["seq"])
	domain=str(json_r["max_conn"])+"-"+json_r["host"]+".facebook.com"
	user=str(json_r["user"])
	partition=str(json_r["partition"])
	user_channel=str(json_r["user_channel"])
	return seq, domain, user, partition, user_channel


###
#print "rev\t"+rev,"c_user\t"+c_user,"sb\t"+sb,"xs\t"+xs,"fr\t"+fr,"pl\t"+pl,"fb_dtsg\t"+fb_dtsg,"seq\t"+seq,"domain\t"+domain,"user\t"+user,"partition\t"+partition,"user_channel\t"+user_channel
###

def pull(domain,user_channel,seq,partition,c_user,xs): #clientid?
	r=requests.get("https://"+domain+"/pull?channel="+user_channel+"&seq="+seq+"&partition="+partition+"&clientid=4efda72a&cb=dawb&idle=2&qp=y&cap=8&pws=fresh&isq=108&msgs_recv=4&uid="+c_user+"&viewer_uid="+c_user+"&sticky_token=851&sticky_pool=frc2c06_chat-proxy&state=active",
		headers={"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64)","Connection": "keep-alive","Cookie":"c_user="+c_user+"; xs="+xs+";"},verify=False)
	#"Connection": "keep-alive" isn't necessary
	#if in respond: application-type:json,but no body is being received by server in /pull then /ajax/presence the whole shit again
	json_r=json.loads(r.text.encode("utf-8")[10:])
	#print json_r
	seq = json_r["seq"]
	return seq,json_r

def msg_check(json_r):
	zaehler=0
	msg=None
	if json_r["t"] == "msg":
		for x in json_r["ms"]:
			if "delta" in json_r["ms"][zaehler].keys():
				if "class" in json_r["ms"][zaehler]["delta"].keys():	
					if json_r["ms"][zaehler]["delta"]["class"]=="NewMessage":
						msg = json_r["ms"][zaehler]["delta"]["body"]
			zaehler=zaehler+1
	return msg
def cancel_friendrequest(fb_dtsg,c_user,xs):
	h={"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64)",
	"Content-Type":"application/x-www-form-urlencoded",
	"Cookie":"c_user="+c_user+"; xs="+xs+";",
	"Proxy-Authorization":"Basic YWk2MDEuYXNhci5tZTp5b2pxcmRuag=="}
	r=requests.post("https://www.facebook.com/ajax/friends/requests/cancel.php",verify=False,data="friend=4&__a=1&fb_dtsg="+fb_dtsg+"&confirmed=1",headers=h) #cancel fs request to zuck lol
	print r.text.encode("utf-8")
def accept_friendrequest(fb_dtsg,c_user,xs):
	h={"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64)",
	"Content-Type":"application/x-www-form-urlencoded",
	"Cookie":"c_user="+c_user+"; xs="+xs+";",
	"Proxy-Authorization":"Basic YWk2MDEuYXNhci5tZTp5b2pxcmRuag=="}
	r=requests.post("https://www.facebook.com/ajax/add_friend/action.php",verify=False,data="to_friend=4&action=add_friend&__a=1&fb_dtsg="+fb_dtsg+"",headers=h) #add zuck as friend
	print r.text.encode("utf-8")


def now():
    return int(time.time()*1000)

def sendMessage():
	timestamp = str(now())
	a="0"
	h={"X-MSGR-Region": "FRC",
	"Connection": "keep-alive",
	"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)",
	"Content-Type": "application/x-www-form-urlencoded",
	"Referer": "https://www.facebook.com/?sk=welcome",
	"Cookie": "c_user="+c_user+"; xs="+xs+";"}
	r=requests.post("https://www.facebook.com/messaging/send/?dpr=1",headers=h,verify=False,data="action_type=ma-type%3Auser-generated-message&body=Hello Mr. Zuckerberg&message_id="+a+"&offline_threading_id="+a+"&other_user_fbid=4&source=source%3Atitan%3Aweb&timestamp="+timestamp+"&__a=1&__rev="+rev+"&fb_dtsg="+fb_dtsg+"").text.encode("utf-8") #do not actually call this function :)
	print r



rev=init()
email=""
password=""
c_user,sb,xs,fr,pl = login(email,password)
fb_dtsg = welcome(c_user,xs)
cancel_friendrequest(fb_dtsg,c_user,xs)
accept_friendrequest(fb_dtsg,c_user,xs)
sendMessage() #somehow this is not working all the time. I need to recheck this.
seq, domain, user, partition, user_channel = pull_zero(c_user,fb_dtsg,xs)
seq,json_r=pull(domain,user_channel,seq,partition,c_user,xs)
try:
	made_ts = json_r["ms"][0]["made"]
	buddyList = json_r["ms"][1]["buddyList"]
except Exception as e:
	print e
msg=msg_check(json_r)
##sticky?, clientid?

